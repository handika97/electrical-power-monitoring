const initialState = {
  status: false,
  values: {},
};

export const listenerReducer = (state, action) => {
  switch (action.type) {
    case "ON":
      return {
        status: true,
      };
    case "OFF":
      return {
        status: false,
      };
    case "VALUES":
      return {
        values: action.values,
      };
    case "RESET_lANG": {
      console.log("action", action.current_lang);
      return action.current_lang;
    }
    default:
      return state;
  }
};
