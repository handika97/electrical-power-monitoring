import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { authContext } from "../context/authSlice";

const mainMenu = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const [value, setValue] = useState([]);
  const [va, setVa] = useState([]);
  const [vb, setVb] = useState([]);
  const [vc, setVc] = useState([]);
  const [Average, setAverage] = useState([]);
  const [perform, setperform] = useState(true);
  useEffect(() => {
    database()
      .ref("dataTemporary")
      .on("child_added", (snapshot) => {
        // if (value.length < 6) {

        value.push({
          Va: snapshot.val().Va,
          Vb: snapshot.val().Vb,
          Vc: snapshot.val().Vc,
          Ia: snapshot.val().Ia,
          Ib: snapshot.val().Ib,
          Ic: snapshot.val().Ic,
          Pa: snapshot.val().Pa,
          Pb: snapshot.val().Pb,
          Pc: snapshot.val().Pc,
          PFa: snapshot.val().PFa,
          PFb: snapshot.val().PFb,
          PFc: snapshot.val().PFc,
          dates: snapshot.val().dates,
        });

        setValue([...value]);
        // }
      });

    return () => {
      database()
      .ref("dataTemporary")
      .off("child_added")
    };
  }, []);
  useEffect(() => {
    setTimeout(() => {
      setperform(false);
    }, 8000);
  }, []);

  return (
    <View style={styles.container}>
      {/* {console.log(value)} */}
      {!perform ? (
        <ScrollView>
          <View
            style={{
              backgroundColor: "#4b3b7a",
              padding: 30,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
              Electrical Power Monitoring
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              // backgroundColor: "black",
              marginTop: 30,
              alignItems: "center",
              justifyContent: "space-between",
              flexDirection: "row",
              paddingHorizontal: 35,
            }}
          >
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("voltage", value)}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/voltage.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Voltage</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("current", value)}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/current.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Current</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("power", value)}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/power.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Power</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              // backgroundColor: "black",
              alignItems: "center",
              justifyContent: "space-between",
              flexDirection: "row",
              paddingHorizontal: 35,
              marginTop: 10,
            }}
          >
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("energy", value)}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/energy.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Energy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("pf", value)}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/powerfaktor.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>
                Power Factor
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => navigation.navigate("maps")}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/maps.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Location</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              // backgroundColor: "black",
              alignItems: "center",
              justifyContent: "space-between",
              flexDirection: "row",
              paddingHorizontal: 35,
              marginTop: 10,
            }}
          >
            <TouchableOpacity
              style={{ flexDirection: "column", alignItems: "center" }}
              onPress={() => {
                dispatch_auth({
                  type: "LOG_OFF",
                }),
                  Heartbeat.stopService();
              }}
            >
              <View style={{ height: 80, width: 80 }}>
                <Image
                  source={require("../asset/sign_out.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <Text style={{ fontWeight: "600", fontSize: 15 }}>Log out</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 25,
              padding: 35,
            }}
          >
            <Text
              style={{
                color: "black",
                fontWeight: "bold",
                fontSize: 37,
              }}
            >
              Track
            </Text>
            <Text
              style={{
                color: "black",
                fontWeight: "bold",
                fontSize: 37,
              }}
            >
              your
            </Text>
            <Text
              style={{
                color: "black",
                fontWeight: "bold",
                fontSize: 37,
              }}
            >
              electricity
            </Text>
            <Text
              style={{
                color: "black",
                fontWeight: "bold",
                fontSize: 37,
              }}
            >
              Easy.
            </Text>
          </View>
        </ScrollView>
      ) : (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator size="large" color="#00ccff" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
