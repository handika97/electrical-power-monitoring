import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ActivityIndicator,
  ImageBackground,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import authh from "@react-native-firebase/auth";
import { authContext } from "../context/authSlice";

const login = ({ navigation, heartBeat }) => {
  const { auth, dispatch_auth } = useContext(authContext);
  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const [Visible, setVisible] = useState(false);
console.log(auth)
  // const login = async () => {
  //   if (Username && Password) {
  //     try {
  //       setVisible(true);
  //       await authh()
  //         .signInWithEmailAndPassword(Username, Password)
  //         .then((res) => {
  //           // alert('User account created & signed in!');
  //           dispatch_auth({
  //             type: "LOGIN",
  //             name: Username.toUpperCase(),
  //           });
  //           setVisible(false);
  //           Heartbeat.startService();
  //         })
  //         .catch((error) => {
  //           setVisible(false);
  //           // if (error.code === "auth/email-already-in-use") {
  //           //   alert("That email address is already in use!");
  //           // }

  //           // if (error.code === "auth/invalid-email") {
  //           alert(error.code);
  //           // }
  //           console.error(error);
  //         });
  //     } catch (error) {
  //       setVisible(false);
  //       // if (error.code === "auth/email-already-in-use") {
  //       //   alert("That email address is already in use!");
  //       // }

  //       // if (error.code === "auth/invalid-email") {
  //       alert(error.code);
  //       // }
  //       console.error(error);
  //     }
  //   }
  // };

  const login = () => {
    if (Username && Password) {
      let index = auth.data?.filter(function(obj) {
        return (
          obj.name.toLowerCase() === Username.toLowerCase() &&
          obj.password === Password
        );
      });
      console.log("index", index);
      if (index.length > 0) {
        dispatch_auth({
          type: "LOGIN",
          name: Username.toUpperCase(),
        });
         Heartbeat.startService();
      } else {
        alert("Username atau Password Salah");
      }
    } else {
      alert("Username atau Password Tidak Boleh Kosong");
    }
    // auth.data.some;
  };
  return (
    <View style={styles.container}>
      {!Visible ? (
        <>
          <ScrollView>
            <View
              style={{
                backgroundColor: "#4b3b7a",
                padding: 30,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
              }}
            >
              <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
                Hello there,
              </Text>
              <Text style={{ fontSize: 22, color: "white" }}>welcome back</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: "white",
                marginTop: 50,
                alignItems: "center",
              }}
            >
              <View style={{ height: 200, width: 200 }}>
                <Image
                  source={require("../asset/logo.png")}
                  style={{ height: "100%", width: "100%" }}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ height: 40, width: 40 }}>
                  <Image
                    source={require("../asset/user1.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <TextInput
                  style={{
                    borderWidth: 2,
                    width: 250,
                    borderRadius: 10,
                    fontSize: 17,
                    height: 40,
                  }}
                  placeholder="Username"
                  value={Username}
                  onChangeText={setUsername}
                />
              </View>
              <View style={{ flexDirection: "row", margin: 20 }}>
                <View style={{ height: 40, width: 40 }}>
                  <Image
                    source={require("../asset/padlock.png")}
                    style={{ height: "100%", width: "100%" }}
                  />
                </View>
                <TextInput
                  style={{
                    borderWidth: 2,
                    width: 250,
                    borderRadius: 10,
                    fontSize: 17,
                    height: 40,
                  }}
                  placeholder="Password"
                  secureTextEntry
                  value={Password}
                  onChangeText={setPassword}
                />
              </View>
            </View>
            <View
              style={{
                alignItems: "center",
                marginTop: 20,
              }}
            >
              <TouchableOpacity onPress={() => login()}>
                <Text
                  style={{
                    backgroundColor: "#4b3b7a",
                    color: "white",
                    fontWeight: "bold",
                    paddingHorizontal: 15,
                    paddingVertical: 10,
                    borderRadius: 5,
                  }}
                >
                  Sign In
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                alignItems: "center",
                marginTop: 20,
              }}
            >
              <TouchableOpacity onPress={() => navigation.navigate("register")}>
                <Text
                  style={{
                    //   backgroundColor: "#4b3b7a",
                    color: "black",
                    fontWeight: "700",
                  }}
                >
                  Don't Have Account To Login?{" "}
                  <Text
                    style={{
                      //   backgroundColor: "#4b3b7a",
                      color: "orange",
                      fontWeight: "700",
                    }}
                  >
                    Sign Up
                  </Text>
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </>
      ) : (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator size="large" color="#00ccff" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(login);
