import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import { Value } from "react-native-reanimated";

// const value = [
//   { Va: 1, dates: 1595013432312 },
//   { Va: 1, dates: 1595013446325 },
//   { Va: 1, dates: 1595013447555 },
//   { Va: 2, dates: 1595023473599 },
//   { Va: 2, dates: 1595023477967 },
//   { Va: 2, dates: 1595023485113 },
//   { Va: 2, dates: 1595023605615 },
//   { Va: 3, dates: 1595033726370 },
//   { Va: 3, dates: 1595033845362 },
//   { Va: 3, dates: 1595033965720 },
//   { Va: 4, dates: 1595044091230 },
//   { Va: 4, dates: 1595044205736 },
//   { Va: 4, dates: 1595044325799 },
//   { Va: 4, dates: 1595044445633 },
// ];
const mainMenu = ({ navigation, heartBeat, route }) => {
  let value = route.params;
  // console.log("h", value);
  // const [value, setValue] = useState([]);
  const [va, setVa] = useState([]);
  const [vb, setVb] = useState([]);
  const [vc, setVc] = useState([]);
  const [loading, setloading] = useState(true);
  const [times, settime] = useState([]);
  const [allData, setallData] = useState([]);
  useEffect(() => {
    let n = value[0].dates;
    let arrayVa = [];
    let arrayVb = [];
    let arrayVc = [];
    let totalVa = 0;
    let totalVb = 0;
    let totalVc = 0;
    let jam = [];
    let jumlahVa = 0;
    let jumlahVb = 0;
    let jumlahVc = 0;
    for (let i = 0; i < value.length; i++) {
      if (new Date(n).getHours() === new Date(value[i].dates).getHours()) {
        totalVa += value[i].Va;
        totalVb += value[i].Vb;
        totalVc += value[i].Vc;
        jumlahVa += 1;
        jumlahVb += 1;
        jumlahVc += 1;
      } else {
        arrayVa = [...arrayVa, totalVa / jumlahVa];
        arrayVb = [...arrayVb, totalVb / jumlahVb];
        arrayVc = [...arrayVc, totalVc / jumlahVc];
        totalVa = value[i].Va;
        totalVb = value[i].Vb;
        totalVc = value[i].Vc;
        jumlahVa = 1;
        jumlahVb = 1;
        jumlahVc = 1;
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }
      if (i === value.length - 1) {
        arrayVa = [...arrayVa, totalVa / jumlahVa];
        arrayVb = [...arrayVb, totalVb / jumlahVb];
        arrayVc = [...arrayVc, totalVc / jumlahVc];
        let newArrayVa = [];
        let newArrayVb = [];
        let newArrayVc = [];
        let newArrayJam = [];
        for (let j = 0; j < arrayVa.length; j++) {
          if (newArrayVa.length <= 6) {
            newArrayVa = [...newArrayVa, arrayVa[arrayVa.length - 1 - j]];
            newArrayVb = [...newArrayVb, arrayVb[arrayVa.length - 1 - j]];
            newArrayVc = [...newArrayVc, arrayVc[arrayVa.length - 1 - j]];
            newArrayJam = [...newArrayJam, jam[arrayVa.length - 1 - j]];
          }
          console.log(newArrayVa);
        }
        newArrayVa = newArrayVa.reverse().slice(0, -1);
        newArrayVb = newArrayVb.reverse().slice(0, -1);
        newArrayVc = newArrayVc.reverse().slice(0, -1);
        newArrayJam = newArrayJam.reverse().slice(1);
        console.log("oke", newArrayVa.slice(0, -1));
        setVa(newArrayVa);
        setVb(newArrayVb);
        setVc(newArrayVc);
        setloading(false);
        settime(newArrayJam);
        let data = [];
        for (let j = 0; j < newArrayVa.length; j++) {
          data = [
            ...data,
            {
              Va: newArrayVa[j],
              Vb: newArrayVb[j],
              Vc: newArrayVc[j],
              jam: newArrayJam[j],
            },
          ];
        }
        console.log(data);
        setallData(data);
      } else if (i === 0) {
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }
      n = value[i].dates;
    }
  }, []);
  return (
    <View style={styles.container}>
      {!loading && (
        <ScrollView>
          <View
            style={{
              backgroundColor: "#4b3b7a",
              padding: 30,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
              Voltage
            </Text>
          </View>
          {/* {console.log(va, vb, vc, times)} */}
          {va && vb && vc && times ? (
            <>
              <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 18 }}>Grafik data</Text>
              </View>
              <LineChart
                data={{
                  labels: times,
                  datasets: [
                    {
                      data: va,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
                    },
                    {
                      data: vb,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 255, 0, ${opacity})`,
                    },
                    {
                      data: vc,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
                    },
                  ],
                }}
                width={Dimensions.get("window").width - 16}
                height={220}
                chartConfig={{
                  backgroundColor: "#c92ac7",
                  backgroundGradientFrom: "#ffffff",
                  backgroundGradientTo: "#ffffff",
                  decimalPlaces: 2,
                  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                  style: {
                    borderRadius: 16,
                  },
                }}
                style={{ marginVertical: 8, borderRadius: 16 }}
              />
              <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 18 }}>Keterangan</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "red",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Va (Volt)</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "blue",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Vb (Volt)</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "green",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Vc (Volt)</Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 20,
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>No</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>jam</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Va</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Vb</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Vc</Text>
                  </View>
                </View>
                {allData.map((item, index) => (
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{index + 1}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.jam}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Va.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Vb.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Vc.toFixed(2)}</Text>
                    </View>
                  </View>
                ))}
              </View>
            </>
          ) : null}
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
