import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  Dimensions,
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import { connect } from "react-redux";

const mainMenu = ({ navigation, heartBeat, route }) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          backgroundColor: "#4b3b7a",
          padding: 30,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
          Location
        </Text>
      </View>
      <View style={{ flex: 1, padding: 10 }}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={{ height: "100%", width: "100%", borderRadius: 10 }}
          region={{
            latitude: -6.97567833,
            longitude: 107.6292637,
            latitudeDelta: 0.00015,
            longitudeDelta: 0.0051,
          }}
          mapType={"satellite"}
        >
          <Marker
            coordinate={{
              latitude: -6.97567833,
              longitude: 107.6292637,
            }}
          />
        </MapView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
