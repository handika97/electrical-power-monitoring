import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";

const mainMenu = ({ navigation, heartBeat }) => {
  console.log("h", heartBeat);
  const [value, setValue] = useState([]);
  // useEffect(() => {
  //   let data = database().ref("dataTemporary");
  //   data.on("child_added", (snapshot) => {
  //     value.push({
  //       Va: snapshot.val().Va,
  //       Vb: snapshot.val().Vb,
  //       Vc: snapshot.val().Vc,
  //       Ia: snapshot.val().Ia,
  //       Ib: snapshot.val().Ib,
  //       Ic: snapshot.val().Ic,
  //       Pa: snapshot.val().Pa,
  //       Pb: snapshot.val().Pb,
  //       Pc: snapshot.val().Pc,
  //       dates: snapshot.val().dates,
  //     });
  //     setValue([...value]);
  //     let Va = [];
  //     let totalVa = 0;
  //     let Vb = [];
  //     let totalVb = 0;
  //     let Vc = [];
  //     let totalVc = 0;
  //     let Ia = [];
  //     let totalIa = 0;
  //     let Ib = [];
  //     let totalIb = 0;
  //     let Ic = [];
  //     let totalIc = 0;
  //     let Pa = [];
  //     let totalPa = 0;
  //     let Pb = [];
  //     let totalPb = 0;
  //     let Pc = [];
  //     let totalPc = 0;

  //     if (value.length === 4) {
  //       for (let i = 0; i < value.length; i++) {
  //         // console.log(value.length);
  //         Va = [...Va, value[i].Va];
  //         Vb = [...Vb, value[i].Vb];
  //         Vc = [...Vc, value[i].Vc];
  //         Ia = [...Ia, value[i].Ia];
  //         Ib = [...Ib, value[i].Ib];
  //         Ic = [...Ic, value[i].Ic];
  //         Pa = [...Pa, value[i].Pa];
  //         Pb = [...Pb, value[i].Pb];
  //         Pc = [...Pc, value[i].Pc];
  //       }
  //       for (let i = 0; i < Va.length; i++) {
  //         totalVa = Va[i] + totalVa;
  //         totalVb = Vb[i] + totalVb;
  //         totalVc = Vc[i] + totalVc;
  //         totalIc = Ic[i] + totalIc;
  //         totalIa = Ia[i] + totalIa;
  //         totalIb = Ib[i] + totalIb;
  //         totalPc = Pc[i] + totalPc;
  //         totalPa = Pa[i] + totalPa;
  //         totalPb = Pb[i] + totalPb;
  //       }
  //       let date = new Date();
  //       let Rata = {
  //         totalVa: totalVa / 4,
  //         totalVb: totalVb / 4,
  //         totalVc: totalVc / 4,
  //         totalIa: totalIa / 4,
  //         totalIb: totalIb / 4,
  //         totalIc: totalIc / 4,
  //         totalPa: totalPa / 4,
  //         totalPb: totalPb / 4,
  //         totalPc: totalPc / 4,
  //         dates: date.getTime(),
  //       };
  //       console.log(Rata);
  //       database()
  //         .ref("data")
  //         .child("")
  //         .push(Rata)
  //         .then(() => {
  //           database()
  //             .ref("dataTemporary")
  //             .remove();
  //         });
  //     }
  //   });

  //   return () => {
  //     data.off();
  //   };
  // }, []);
  const { list, dispatch_listener } = useContext(listenerContext);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View
          style={{
            backgroundColor: "#4b3b7a",
            padding: 30,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
          }}
        >
          <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
            Hello there,
          </Text>
          <Text style={{ fontSize: 22, color: "white" }}>welcome back</Text>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            marginTop: 80,
            alignItems: "center",
          }}
        >
          <View style={{ height: 200, width: 200 }}>
            <Image
              source={require("../asset/logo.png")}
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ height: 40, width: 40 }}>
              <Image
                source={require("../asset/user1.png")}
                style={{ height: "100%", width: "100%" }}
              />
            </View>
            <TextInput
              style={{
                borderWidth: 2,
                width: 250,
                borderRadius: 10,
                fontSize: 17,
                height: 40,
              }}
              placeholder="Username"
            />
          </View>
          <View style={{ flexDirection: "row", margin: 20 }}>
            <View style={{ height: 40, width: 40 }}>
              <Image
                source={require("../asset/padlock.png")}
                style={{ height: "100%", width: "100%" }}
              />
            </View>
            <TextInput
              style={{
                borderWidth: 2,
                width: 250,
                borderRadius: 10,
                fontSize: 17,
                height: 40,
              }}
              placeholder="Password"
              secureTextEntry
            />
          </View>
        </View>
        <TouchableOpacity
          style={{
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <Text
            style={{
              backgroundColor: "#4b3b7a",
              color: "white",
              fontWeight: "bold",
              paddingHorizontal: 15,
              paddingVertical: 10,
              borderRadius: 5,
            }}
          >
            Sign In
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
