import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import { Value } from "react-native-reanimated";

// const value = [
//   { Va: 1, dates: 1595013432312 },
//   { Va: 1, dates: 1595013446325 },
//   { Va: 1, dates: 1595013447555 },
//   { Va: 2, dates: 1595023473599 },
//   { Va: 2, dates: 1595023477967 },
//   { Va: 2, dates: 1595023485113 },
//   { Va: 2, dates: 1595023605615 },
//   { Va: 3, dates: 1595033726370 },
//   { Va: 3, dates: 1595033845362 },
//   { Va: 3, dates: 1595033965720 },
//   { Va: 4, dates: 1595044091230 },
//   { Va: 4, dates: 1595044205736 },
//   { Va: 4, dates: 1595044325799 },
//   { Va: 4, dates: 1595044445633 },
// ];
const mainMenu = ({ navigation, heartBeat, route }) => {
  let value = route.params;
  console.log("h", value);
  // const [value, setValue] = useState([]);
  const [Ia, setIa] = useState([]);
  const [Ib, setIb] = useState([]);
  const [Ic, setIc] = useState([]);
  const [loading, setloading] = useState(true);
  const [times, settime] = useState([]);
  const [allData, setallData] = useState([]);

  useEffect(() => {
    let n = value[0].dates;
    let arrayIa = [];
    let arrayIb = [];
    let arrayIc = [];
    let totalIa = 0;
    let totalIb = 0;
    let totalIc = 0;
    let jam = [];
    let jumlahIa = 0;
    let jumlahIb = 0;
    let jumlahIc = 0;
    for (let i = 0; i < value.length; i++) {
      if (new Date(n).getHours() === new Date(value[i].dates).getHours()) {
        totalIa += value[i].Ia;
        totalIb += value[i].Ib;
        totalIc += value[i].Ic;
        jumlahIa += 1;
        jumlahIb += 1;
        jumlahIc += 1;
      } else {
        arrayIa = [...arrayIa, totalIa / jumlahIa];
        arrayIb = [...arrayIb, totalIb / jumlahIb];
        arrayIc = [...arrayIc, totalIc / jumlahIc];
        totalIa = value[i].Ia;
        totalIb = value[i].Ib;
        totalIc = value[i].Ic;
        jumlahIa = 1;
        jumlahIb = 1;
        jumlahIc = 1;
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }

      if (i === value.length - 1) {
        arrayIa = [...arrayIa, totalIa / jumlahIa];
        arrayIb = [...arrayIb, totalIb / jumlahIb];
        arrayIc = [...arrayIc, totalIc / jumlahIc];
        let newArrayIa = [];
        let newArrayIb = [];
        let newArrayIc = [];
        let newArrayJam = [];
        for (let j = 0; j < arrayIa.length; j++) {
          if (newArrayIa.length <= 6) {
            newArrayIa = [...newArrayIa, arrayIa[arrayIa.length - 1 - j]];
            newArrayIb = [...newArrayIb, arrayIb[arrayIa.length - 1 - j]];
            newArrayIc = [...newArrayIc, arrayIc[arrayIa.length - 1 - j]];
            newArrayJam = [...newArrayJam, jam[arrayIa.length - 1 - j]];
          }
          console.log(newArrayIa);
        }
        newArrayIa = newArrayIa.reverse().slice(0, -1);
        newArrayIb = newArrayIb.reverse().slice(0, -1);
        newArrayIc = newArrayIc.reverse().slice(0, -1);
        newArrayJam = newArrayJam.reverse().slice(1);
        console.log("oke", newArrayIa.slice(0, -1));
        setIa(newArrayIa);
        setIb(newArrayIb);
        setIc(newArrayIc);
        setloading(false);
        settime(newArrayJam);
        let data = [];
        for (let j = 0; j < newArrayIa.length; j++) {
          data = [
            ...data,
            {
              Ia: newArrayIa[j],
              Ib: newArrayIb[j],
              Ic: newArrayIc[j],
              jam: newArrayJam[j],
            },
          ];
        }
        console.log(data);
        setallData(data);
      } else if (i === 0) {
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }
      n = value[i].dates;
      console.log(arrayIa, arrayIb, arrayIc, jam);
    }
  }, []);
  return (
    <View style={styles.container}>
      {!loading && (
        <ScrollView>
          {/* {console.log(value)} */}
          <View
            style={{
              backgroundColor: "#4b3b7a",
              padding: 30,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
              Current
            </Text>
          </View>
          {/* {console.log(va, vb, vc, times)} */}
          {Ia && Ib && Ic && times ? (
            <>
              <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 18 }}>Grafik data</Text>
              </View>
              <LineChart
                data={{
                  labels: times,
                  datasets: [
                    {
                      data: Ia,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
                    },
                    {
                      data: Ib,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 255, 0, ${opacity})`,
                    },
                    {
                      data: Ic,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
                    },
                  ],
                }}
                width={Dimensions.get("window").width - 16}
                height={220}
                chartConfig={{
                  backgroundColor: "#c92ac7",
                  backgroundGradientFrom: "#ffffff",
                  backgroundGradientTo: "#ffffff",
                  decimalPlaces: 2,
                  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                  style: {
                    borderRadius: 16,
                  },
                }}
                style={{ marginVertical: 8, borderRadius: 16 }}
              />
              <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 18 }}>Keterangan</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "red",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Ia (Ampere)</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "blue",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Ib (Ampere)</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "green",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>Ic (Ampere)</Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 20,
                }}
              >
                {/* <TouchableOpacity>
                  <Text
                    style={{
                      backgroundColor: "#4b3b7a",
                      color: "white",
                      fontWeight: "bold",
                      paddingHorizontal: 15,
                      paddingVertical: 10,
                      borderRadius: 5,
                    }}
                  >
                    Tabel
                  </Text>
                </TouchableOpacity> */}
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>No</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>jam</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Ia</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Ib</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>Ic</Text>
                  </View>
                </View>
                {allData.map((item, index) => (
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{index + 1}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.jam}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Ia.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Ib.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Ic.toFixed(2)}</Text>
                    </View>
                  </View>
                ))}
              </View>
            </>
          ) : null}
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
