import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  ImageBackground,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import Heartbeat from "../../Heartbeat";
import database from "@react-native-firebase/database";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ScrollView, TextInput } from "react-native-gesture-handler";
import { listenerContext } from "../context/listener";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import { Value } from "react-native-reanimated";

// const value = [
//   { Va: 1, dates: 1595013432312 },
//   { Va: 1, dates: 1595013446325 },
//   { Va: 1, dates: 1595013447555 },
//   { Va: 2, dates: 1595023473599 },
//   { Va: 2, dates: 1595023477967 },
//   { Va: 2, dates: 1595023485113 },
//   { Va: 2, dates: 1595023605615 },
//   { Va: 3, dates: 1595033726370 },
//   { Va: 3, dates: 1595033845362 },
//   { Va: 3, dates: 1595033965720 },
//   { Va: 4, dates: 1595044091230 },
//   { Va: 4, dates: 1595044205736 },
//   { Va: 4, dates: 1595044325799 },
//   { Va: 4, dates: 1595044445633 },
// ];
const mainMenu = ({ navigation, heartBeat, route }) => {
  let value = route.params;
  console.log("h", value);
  // const [value, setValue] = useState([]);
  const [Pa, setPa] = useState([]);
  const [Pb, setPb] = useState([]);
  const [Pc, setPc] = useState([]);
  const [loading, setloading] = useState(true);
  const [times, settime] = useState([]);
  const [allData, setallData] = useState([]);

  useEffect(() => {
    let n = value[0].dates;
    let arrayPa = [];
    let arrayPb = [];
    let arrayPc = [];
    let totalPa = 0;
    let totalPb = 0;
    let totalPc = 0;
    let jam = [];
    let jumlahPa = 0;
    let jumlahPb = 0;
    let jumlahPc = 0;
    for (let i = 0; i < value.length; i++) {
      // console.log(new Date(1595044205736).getHours());
      //   if (
      //     new Date(n).getFullYear() === new Date(value[i].dates).getFullYear()
      //   ) {
      //     if (new Date(n).getMonth() === new Date(value[i].dates).getMonth()) {
      //       if (new Date(n).getDate() === new Date(value[i].dates).getDate()) {
      if (new Date(n).getHours() === new Date(value[i].dates).getHours()) {
        totalPa += value[i].PFa;
        totalPb += value[i].PFb;
        totalPc += value[i].PFc;
        jumlahPa += 1;
        jumlahPb += 1;
        jumlahPc += 1;
      } else {
        // console.log(new Date(value[P].dates).getHours(), "gantP");
        arrayPa = [...arrayPa, totalPa / jumlahPa];
        arrayPb = [...arrayPb, totalPb / jumlahPb];
        arrayPc = [...arrayPc, totalPc / jumlahPc];
        totalPa = value[i].PFa;
        totalPb = value[i].PFb;
        totalPc = value[i].PFc;
        jumlahPa = 1;
        jumlahPb = 1;
        jumlahPc = 1;
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }
      //       }
      //     }
      //   }
      if (i === value.length - 1) {
        arrayPa = [...arrayPa, totalPa / jumlahPa];
        arrayPb = [...arrayPb, totalPb / jumlahPb];
        arrayPc = [...arrayPc, totalPc / jumlahPc];
        let newArrayPa = [];
        let newArrayPb = [];
        let newArrayPc = [];
        let newArrayJam = [];
        for (let j = 0; j < arrayPa.length; j++) {
          if (newArrayPa.length <= 6) {
            newArrayPa = [...newArrayPa, arrayPa[arrayPa.length - 1 - j]];
            newArrayPb = [...newArrayPb, arrayPb[arrayPa.length - 1 - j]];
            newArrayPc = [...newArrayPc, arrayPc[arrayPa.length - 1 - j]];
            newArrayJam = [...newArrayJam, jam[arrayPa.length - 1 - j]];
          }
          console.log(newArrayPa);
        }
        newArrayPa = newArrayPa.reverse().slice(0, -1);
        newArrayPb = newArrayPb.reverse().slice(0, -1);
        newArrayPc = newArrayPc.reverse().slice(0, -1);
        newArrayJam = newArrayJam.reverse().slice(1);
        console.log("oke", newArrayPa.slice(0, -1));
        setPa(newArrayPa);
        setPb(newArrayPb);
        setPc(newArrayPc);
        setloading(false);
        settime(newArrayJam);
        let data = [];
        for (let j = 0; j < newArrayPa.length; j++) {
          data = [
            ...data,
            {
              Pa: newArrayPa[j],
              Pb: newArrayPb[j],
              Pc: newArrayPc[j],
              jam: newArrayJam[j],
            },
          ];
        }
        console.log(data);
        setallData(data);
      } else if (i === 0) {
        jam = [...jam, `${new Date(value[i].dates).getHours()}`];
      }
      n = value[i].dates;
      //   console.log(arrayIa, arrayIb, arrayIc, jam);
    }
  }, []);
  return (
    <View style={styles.container}>
      {!loading && (
        <ScrollView>
          {/* {console.log(value)} */}

          <View
            style={{
              backgroundColor: "#4b3b7a",
              padding: 30,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 22, color: "white", marginTop: 22 }}>
              Power Factor
            </Text>
          </View>
          {/* {console.log(va, Pb, vc, times)} */}
          {Pa && Pb && Pc && times ? (
            <>
              <View>
                <Text>Grafik data</Text>
              </View>
              <LineChart
                data={{
                  labels: times,
                  datasets: [
                    {
                      data: Pa,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
                    },
                    {
                      data: Pb,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 255, 0, ${opacity})`,
                    },
                    {
                      data: Pc,
                      strokeWidth: 2,
                      color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
                    },
                  ],
                }}
                width={Dimensions.get("window").width - 16}
                height={220}
                chartConfig={{
                  backgroundColor: "#c92ac7",
                  backgroundGradientFrom: "#ffffff",
                  backgroundGradientTo: "#ffffff",
                  decimalPlaces: 2,
                  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                  style: {
                    borderRadius: 16,
                  },
                }}
                style={{ marginVertical: 8, borderRadius: 16 }}
              />
              <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 18 }}>Keterangan</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "red",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>PFa</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "blue",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>PFb</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                }}
              >
                <View
                  style={{
                    width: 70,
                    borderBottomWidth: 2,
                    borderBottomColor: "green",
                    marginRight: 20,
                  }}
                />
                <Text style={{ fontSize: 18 }}>PFc</Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  padding: 20,
                }}
              >
                {/* <TouchableOpacity>
                  <Text
                    style={{
                      backgroundColor: "#4b3b7a",
                      color: "white",
                      fontWeight: "bold",
                      paddingHorizontal: 15,
                      paddingVertical: 10,
                      borderRadius: 5,
                    }}
                  >
                    Tabel
                  </Text>
                </TouchableOpacity> */}
                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>No</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>jam</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>PFa</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>PFb</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.2,
                      alignItems: "center",
                      justifyContent: "center",
                      borderWidth: 1,
                    }}
                  >
                    <Text>PFc</Text>
                  </View>
                </View>
                {allData.map((item, index) => (
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{index + 1}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.jam}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Pa.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Pb.toFixed(2)}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.2,
                        alignItems: "center",
                        justifyContent: "center",
                        borderWidth: 1,
                      }}
                    >
                      <Text>{item.Pc.toFixed(2)}</Text>
                    </View>
                  </View>
                ))}
              </View>
            </>
          ) : null}
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
  },
  view: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "gray",
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: "white",
  },
});

const mapStateToProps = (store) => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(mainMenu);
