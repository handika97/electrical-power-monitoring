import axios from'axios';

export default axios.create({
    baseURL: 'https://platform.antares.id:8443/~/antares-cse/antares-id/',
    timeout: 10000,
    headers: {
        'X-M2M-Origin': 'f67b544e770ac646:62c01e7b8af63579',
        'Content-Type': 'application/json;ty=4',
        'Accept':'application/json'
    },
});