import React, { useContext, useEffect, useState } from "react";
import { Image, View, StatusBar, Button, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { SafeAreaProvider } from "react-native-safe-area-context";
// import mainMenu from "../screen/menu";
// import homeLogin from "../screen/homeLogin";
// import register from "../screen/register";
import login from "../screen/login";
import register from "../screen/register";
import splash from "../screen/splash";
import voltage from "../screen/voltage";
import current from "../screen/current";
import power from "../screen/power";
import energy from "../screen/Energy";
import pf from "../screen/PFactor";
import mainMenu from "../screen/mainMenu";
import maps from "../screen/maps";
import { authContext } from "../context/authSlice";
import { listenerContext } from "../context/listener";

const StackApp = createStackNavigator();

const navOptionHandler = () => ({
  headerShown: false,
  tabBarOptions: { activeTintColor: "#F9BA9B" },
  animationEnabled: "false",
});

export default function MainNavigator() {
  const { auth, dispatch_auth } = useContext(authContext);
  const [perform, setperform] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setperform(false);
    }, 2000);
  }, []);
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <StackApp.Navigator>
          {perform && (
            <StackApp.Screen
              name="splash"
              component={splash}
              options={navOptionHandler}
            />
          )}
          {!auth.isLogin ? (
            <>
              <StackApp.Screen
                name="login"
                component={login}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="register"
                component={register}
                options={navOptionHandler}
              />
            </>
          ) : (
            <>
              <StackApp.Screen
                name="mainMenu"
                component={mainMenu}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="voltage"
                component={voltage}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="current"
                component={current}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="power"
                component={power}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="energy"
                component={energy}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="pf"
                component={pf}
                options={navOptionHandler}
              />
              <StackApp.Screen
                name="maps"
                component={maps}
                options={navOptionHandler}
              />
            </>
          )}
        </StackApp.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
