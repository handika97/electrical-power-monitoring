import { AppRegistry, Alert } from "react-native";
import React, { useContext } from "react";
import { Provider } from "react-redux";
import App from "./App";
import { name as appName } from "./app.json";
import { setHeartBeat, store } from "./store";
import database from "@react-native-firebase/database";
import api from "./src/api/apiAntares";
import axios from "axios";
import { listenerContext } from "./src/context/listener";

const MyHeadlessTask = async () => {
  console.log("looping");
  const url = `https://platform.antares.id:8443/~/antares-cse/antares-id/EnergiListrik/MonitoringListrik/la`;
  axios
    .get(url, {
      headers: {
        "X-M2M-Origin": "987d76f16e945264:ed5d313fe58ce18b",
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
    .then((res) => {
      let nilai = JSON.parse(res.data["m2m:cin"].con);
      console.log("res", res.data["m2m:cin"].con);
      console.log("nilai", nilai);

      if (nilai.Va >= 0 && nilai.Ia >= 0 && nilai.Pa >= 0) {
        console.log("masuj");
        let date = new Date();
        database()
          .ref("dataTemporary")
          .child("")
          .push({ ...nilai, dates: date.getTime() });
      }
    })
    .catch((e) => {
      console.log("error get", e);
    });
};

const RNRedux = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerHeadlessTask("Heartbeat", () => MyHeadlessTask);
AppRegistry.registerComponent(appName, () => RNRedux);
